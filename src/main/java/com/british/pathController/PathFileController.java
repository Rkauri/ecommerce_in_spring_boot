package com.british.pathController;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.british.catagory.model.Catagory;
import com.british.catagory.model.SubCatagory;
import com.british.catagory.service.CatagoryService;
import com.british.order.model.Order;
import com.british.order.model.OrderDetail;
import com.british.order.service.OrderService;
import com.british.product.model.Product;
import com.british.product.service.ProductService;
import com.british.user.model.User;
import com.british.user.service.UserService;

@Controller
public class PathFileController {
	@Autowired
	CatagoryService catagoryService;
	@Autowired
	ProductService productService;
	@Autowired
	UserService userService;
	@Autowired
	OrderService orderService;
	public static List<Long> sess_val;

	@RequestMapping(value = { "/", "/index" })
	public String indexPage(ModelMap model) {
		model.addAttribute("catagorylist", catagoryService.catagorylist());
		model.addAttribute("subcatagorylist", catagoryService.subCatagoryList());
		model.addAttribute("productlist", productService.getProducts());
		model.addAttribute("latest", productService.findLatestProduct());
		return "client/index";
	}

	@RequestMapping(value = "/products/{id}", method = RequestMethod.GET)
	public String product(@PathVariable("id") SubCatagory subCatagory, ModelMap model) {
		model.addAttribute("subcatagorylist", catagoryService.subCatagoryList());
		model.addAttribute("productlist", productService.getProductLBySubCatagory(subCatagory));
		return "client/product";
	}

	@RequestMapping(value = "/product_detail/{id}", method = RequestMethod.GET)
	public String productDetail(@PathVariable("id") Long productId, ModelMap model, HttpSession session) {
		model.addAttribute("catagorylist", catagoryService.catagorylist());
		model.addAttribute("subcatagorylist", catagoryService.subCatagoryList());
		model.addAttribute("product", productService.getProductById(productId));
		// model.addAttribute("hello", session.getAttribute("hello"));
		return "client/product_detail";
	}

	@RequestMapping(value = "/cart", method = RequestMethod.POST)
	@ResponseBody
	public HashMap<String, String> addCart(@RequestParam("pid") Long pid, @RequestParam("qty") int qty,
			HttpSession session) {
		ArrayList<Product> p = null;
		HashMap<String, String> hmap = new HashMap<String, String>();
		boolean status = false;
		Product obj = new Product();
		obj.setProductId(pid);
		obj.setQuantity(qty);
		// obj.setProductSize(size);
		if (session.getAttribute("product") == null) {
			p = new ArrayList<Product>();
		} else {
			p = (ArrayList<Product>) session.getAttribute("product");
		}
		for (Product product : p) {
			if (product.getProductId() != null && product.getProductId() == obj.getProductId()) {
				status = true;
				break;
			}
		}
		if (status) {
			hmap.put("msg", "This product already exists in your Cart");
			return hmap;
		} else {
			p.add(obj);
			session.setAttribute("product", p);
			int val = p.size();
			hmap.put("msg", String.valueOf(val));
			return hmap;
		}
	}

	@RequestMapping(value = "/mycart", method = RequestMethod.POST)
	@ResponseBody
	public HashMap<String, String> updateCart(@RequestParam("pid") Long pid, @RequestParam("qty") int qty,
			HttpServletRequest request) {
		HttpSession session = request.getSession(false);
		ArrayList<Product> p = null;
		HashMap<String, String> hmap = new HashMap<String, String>();
		boolean status = false;
		Product obj = new Product();
		obj.setProductId(pid);
		obj.setQuantity(qty);
		// obj.setProductSize(size);
		if (session.getAttribute("product") == null) {
			p = new ArrayList<Product>();
		} else {
			p = (ArrayList<Product>) session.getAttribute("product");
		}
		for (Product product : p) {
			if (product.getProductId() != null && product.getProductId() == obj.getProductId()) {
				p.remove(product);
				p.add(obj);
				status = true;
				break;
			}
		}
		session.setAttribute("product", p);
		if (status) {
			hmap.put("msg", "Updated Successfully");
			return hmap;
		} else {
			hmap.put("msg", "Cannot Update");
			return hmap;
		}
	}

	@RequestMapping(value = "/deleteFromCart", method = RequestMethod.POST)
	@ResponseBody
	public HashMap<String, String> deleteFromCart(@RequestParam("pid") Long pid, @RequestParam("qty") int qty,
			HttpServletRequest request) {
		HttpSession session = request.getSession(false);
		ArrayList<Product> p = null;
		HashMap<String, String> hmap = new HashMap<String, String>();
		boolean status = false;
		Product obj = new Product();
		obj.setProductId(pid);
		obj.setQuantity(qty);
		// obj.setProductSize(size);
		if (session.getAttribute("product") == null) {
			p = new ArrayList<Product>();
		} else {
			p = (ArrayList<Product>) session.getAttribute("product");
		}
		for (Product product : p) {
			if (product.getProductId() != null && product.getProductId() == obj.getProductId()) {
				p.remove(product);
				status = true;
				break;
			}
		}
		session.setAttribute("product", p);
		if (status) {
			hmap.put("msg", "Deleted Successfully");
			return hmap;
		} else {
			hmap.put("msg", "Cannot Delete");
			return hmap;
		}
	}

	@RequestMapping(value = "/cart_detail", method = RequestMethod.GET)
	public String cartDetail(ModelMap model, HttpSession session) {
		List<Product> products = new ArrayList<Product>();
		ArrayList<Product> plist = (ArrayList<Product>) session.getAttribute("product");
		if (plist != null) {
			for (Product product : plist) {
				Product p1 = productService.getProductById(product.getProductId());
				p1.setQuantity(product.getQuantity());
				products.add(p1);
			}
		}
		model.addAttribute("productlist", products);

		return "client/cart";

	}

	@RequestMapping(value = "/checkout", method = RequestMethod.GET)
	public String checkout() {
		return "client/checkout";
	}

	@RequestMapping(value = "/removesesion")
	public void remove(HttpSession session) {
		session.removeAttribute("product");
	}

	@RequestMapping(value = "/addOrder", method = RequestMethod.POST)
	public String addOrder(@RequestParam("detail") String detail, ModelMap model, HttpSession session,
			Authentication auth) {
		ArrayList<Product> plist = (ArrayList<Product>) session.getAttribute("product");
		Order order = new Order();
		order.setOrderDate(new Date());
		order.setStatus("pending");
		User u = userService.findByUsername(auth.getName());
		order.setUserId(u);
		Order dbmodel = orderService.addOrder(order);
		if (dbmodel.getOrderId() != null) {
			for (Product p : plist) {
				OrderDetail d = new OrderDetail();
				d.setOrderDate(new Date());
				d.setProductlist(p);
				d.setOrderId(dbmodel);
				d.setQuantity(p.getQuantity());
				orderService.addOrderDetail(d);

			}
		}
		session.removeAttribute("produdct");
		model.addAttribute("catagorylist", catagoryService.catagorylist());
		model.addAttribute("subcatagorylist", catagoryService.subCatagoryList());

		return "redirect:order";
	}

	@RequestMapping(value = "/products/c/{id}", method = RequestMethod.GET)
	public String byCatagory(@PathVariable("id") Long id, ModelMap model) {
		Catagory c = new Catagory();
		c.setCatagoryId(id);
		List<Product> list = productService.findByCatagory(c);
		model.addAttribute("productlist", list);
		model.addAttribute("catagorylist", catagoryService.catagorylist());
		model.addAttribute("subcatagorylist", catagoryService.subCatagoryList());
		model.addAttribute("totalcount", list.size());
		return "client/product";
	}

	@RequestMapping(value = "/products/sc/{id}", method = RequestMethod.GET)
	public String bySubCatagory(@PathVariable("id") Long id, ModelMap model) {
		SubCatagory c = new SubCatagory();
		c.setSubCatagoryId(id);
		List<Product> list = productService.findBySubCatagory(c);
		model.addAttribute("productlist", list);
		model.addAttribute("catagorylist", catagoryService.catagorylist());
		model.addAttribute("subcatagorylist", catagoryService.subCatagoryList());
		model.addAttribute("totalcount", list.size());
		return "client/product";
	}

	@RequestMapping(value = "/products/sc/{id}/page/{pno}", method = RequestMethod.GET)
	public String pagination(@PathVariable("id") Long id, @PathVariable("pno") Long no, ModelMap model) {
		List<Product> list;
		int pagvalue;
		SubCatagory c = new SubCatagory();
		c.setSubCatagoryId(id);
		if (no != 0) {
			Long lastindex = 9 * no;
			Long firstindex = lastindex - 9 + 1;
			list = productService.getBySubCatagory(id, firstindex, lastindex);
			if (list.size() / 9 < 1) {
				pagvalue = 1;
			} else {
				pagvalue = list.size();
			}
		} else {
			list = productService.findBySubCatagory(c);
			if (list.size() / 9 < 1) {
				pagvalue = 1;
			} else {
				pagvalue = list.size();
			}
		}
		model.addAttribute("productlist", list);
		model.addAttribute("catagorylist", catagoryService.catagorylist());
		model.addAttribute("subcatagorylist", catagoryService.subCatagoryList());
		model.addAttribute("totalcount", pagvalue);
		return "client/product";
	}
	@RequestMapping(value="/search",method=RequestMethod.POST)
	public @ResponseBody List<Product> getSearchData(@RequestParam("product") String productname){
		List<Product> list= productService.getProductSearch(productname);
		return list;
	}
}
