package com.british.catagoryController;

import java.util.List;

import javax.servlet.ServletContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.british.catagory.model.Catagory;
import com.british.catagory.model.SubCatagory;
import com.british.catagory.service.CatagoryService;

@Controller
@RequestMapping(value = "/admin")
public class CatagoryController {
	@Autowired
	private CatagoryService catagoryService;
	@Autowired
	ServletContext context;

	@RequestMapping(value = "/catagory", method = RequestMethod.GET)
	public String catagory() {
		return "catagory/addCatagory";
	}

	@RequestMapping(value = "/catagorylist", method = RequestMethod.GET)
	public String catagorylist(ModelMap model) {
		model.addAttribute("catagorylist", catagoryService.catagorylist());
		return "catagory/catagorylist";
	}

	@RequestMapping(value = "/subCatagory", method = RequestMethod.GET)
	public String subCatagory(ModelMap model) {
		model.addAttribute("catagorylist", catagoryService.catagorylist());
		return "catagory/addSubCatagory";
	}

	@RequestMapping(value = "/subCatagoryByCatagoryId", method = RequestMethod.GET)
	public @ResponseBody List<SubCatagory> getSubCatagoryListByCatgoryId(@RequestParam("id") Long id) {
		Catagory catagory = new Catagory();
		catagory.setCatagoryId(id);
		List<SubCatagory> slist = catagoryService.getByCatagoryId(catagory);
		return slist;
	}
	// @RequestMapping(value="/childcatagory")
	// public String childCatagory(ModelMap model) {
	// model.addAttribute("subcatagorylist",catagoryService.subCatagoryList());
	// return "catagory/addChildCatagory";
	// }

	@RequestMapping(value = "/catagory_insert", method = RequestMethod.POST)
	public String insertCatagory(@ModelAttribute Catagory catagory, RedirectAttributes model) {

		if (catagoryService.addCatagory(catagory)) {
			model.addFlashAttribute("success", "Action Processed");
		} else {
			// System.out.println(context.getRealPath("resources/static/upload"));
			model.addFlashAttribute("failure", "Action Process Failed");
		}

		return "redirect:catagory";
	}

	@RequestMapping(value = "subcatagory_insert", method = RequestMethod.POST)
	public String insertSubCatagory(@ModelAttribute SubCatagory subCatagory, RedirectAttributes model, ModelMap map) {
		if (catagoryService.addSubCatagory(subCatagory)) {
			model.addFlashAttribute("success", "Action Processed");
		} else {
			model.addFlashAttribute("failure", "Action Processed Failed");
		}
		map.addAttribute("catagorylist", catagoryService.catagorylist());
		return "catagory/addSubCatagory";
	}

	@RequestMapping(value = "subcatagorylist", method = RequestMethod.GET)
	public String subcatagoryList(ModelMap model) {
		model.addAttribute("subcatagorylist", catagoryService.subCatagoryList());
		return "catagory/subcatagorylist";
	}
	// @RequestMapping(value="childcatagory_insert")
	// public String insertChildCatagory(@ModelAttribute ChildCatagory
	// childCatagory,RedirectAttributes model,ModelMap map) {
	// if(catagoryService.addChildCatagory(childCatagory)) {
	// model.addFlashAttribute("success", "Action Processed");
	// }else {
	// model.addFlashAttribute("failure", "Action Processed Failed");
	// }
	// map.addAttribute("subcatagorylist",catagoryService.subCatagoryList());
	// return "catagory/addChildCatagory";
	// }
	// @RequestMapping(value="childcatagorylist")
	// public String childcatagoryList(ModelMap model) {
	// model.addAttribute("childcatagorylist",catagoryService.childCatagoryList());
	// return "catagory/childcatagorylist";
	// }

}
