package com.british.product.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.british.catagory.model.Catagory;
import com.british.catagory.model.SubCatagory;
import com.british.product.model.Product;

@Repository
public interface ProductRepository extends JpaRepository<Product, Long> {
	public Product findByProductId(Long productId);

	public List<Product> findBySCatagory(SubCatagory subCatagory);

	@Query(name = "select \r\n" + 
			"PRODUCT_ID ,\r\n" + 
			"CREATED_BY ,\r\n" + 
			"CREATED_DT,\r\n" + 
			"DESCRIPTION,\r\n" + 
			"IMG_PATH,\r\n" + 
			"PRICE,\r\n" + 
			"PRODUCT_COLOR,\r\n" + 
			"PRODUCT_NAME,\r\n" + 
			"PRODUCT_SIZE,\r\n" + 
			"QUANTITY ,\r\n" + 
			"UPDATED_BY,\r\n" + 
			"UPDATED_DT,\r\n" + 
			"CATAGORY_CATAGORY_ID,\r\n" + 
			"S_CATAGORY_SUB_CATAGORY_ID   from(select p.*,rownum r from tbl_product p) where r between ?2 and ?3", nativeQuery = true)
	public List<Product> getBySCatagory(Long id, Long firstindex, Long lastindex);

	@Query(value = "select * from tbl_product order by created_dt", nativeQuery = true)
	public List<Product> findLatestProduct();

	public List<Product> findByCatagory(Catagory catagory);
	
	@Query(name="select * from tbl_product where upper(product_name)  LIKE CONCAT('%',upper(:productname),'%')",nativeQuery=true)
	public List<Product> findByProductNameContaining(String productname);
	

}