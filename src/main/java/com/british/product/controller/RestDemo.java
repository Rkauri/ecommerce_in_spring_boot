package com.british.product.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import com.british.catagory.model.Catagory;
import com.british.catagory.service.CatagoryService;

@RestController
public class RestDemo {
	@Autowired
	private CatagoryService catagoryService;
	
	@RequestMapping(value="/Demo",method=RequestMethod.GET)
	public ResponseEntity<List<Catagory>> getData() {
		List<Catagory> enn = catagoryService.catagorylist();
	return new ResponseEntity<List<Catagory>>(enn,HttpStatus.OK);
	}
	@RequestMapping(value="/Demo/{catagoryId}",method=RequestMethod.GET)
	public ResponseEntity<Catagory> getCatagory(@PathVariable("catagoryId") Long id) {
		Catagory c= catagoryService.getByCatagoryKey(id);
		return new ResponseEntity<Catagory>(c,HttpStatus.OK);
	}
	@RequestMapping(value="/Demo",method=RequestMethod.POST,consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> postCatagory(@RequestBody Catagory catagory,UriComponentsBuilder ucBuilder) {
		
        catagoryService.addCatagory(catagory);
 
        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(ucBuilder.path("/Demo/{catagoryId}").buildAndExpand(catagory.getCatagoryId()).toUri());
        return new ResponseEntity<String>(headers, HttpStatus.CREATED);
	}
	

}
