package com.british.product.controller;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Date;

import javax.activation.MimetypesFileTypeMap;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.british.catagory.model.Catagory;
import com.british.catagory.model.SubCatagory;
import com.british.catagory.service.CatagoryService;
import com.british.product.model.Product;
import com.british.product.service.ProductService;

@Controller
@RequestMapping(value = "/admin")
public class ProductController {
	@Autowired
	private ProductService productService;
	@Autowired
	private CatagoryService catagoryService;
	@Autowired
	ServletContext context;

	@RequestMapping(value = "/product", method = RequestMethod.GET)
	public String product(ModelMap model) {
		model.addAttribute("catagorylist", catagoryService.catagorylist());
		model.addAttribute("subcatagorylist", catagoryService.subCatagoryList());
		// model.addAttribute("childcatagorylist",catagoryService.childCatagoryList());
		return "product/productform";
	}

	@RequestMapping(value = "/addProduct", method = RequestMethod.POST)
	public String addProduct(@ModelAttribute Product product, @RequestParam("file") MultipartFile file,
			RedirectAttributes model, HttpServletRequest request) {

		String path = request.getSession().getServletContext().getRealPath("src/main/resources/static/upload");
		String filePath = "upload/" + file.getOriginalFilename();
		File f = new File(filePath);
		String mimetype = new MimetypesFileTypeMap().getContentType(f);
		if (mimetype.startsWith("image/")) {
			System.out.println("It's an image");
			try {

				byte[] bytes = file.getBytes();
				OutputStream os = new FileOutputStream("src/main/resources/static/" + filePath);
				for (int x = 0; x < bytes.length; x++) {
					os.write(bytes[x]); // writes the bytes
				}
				os.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else {
			model.addFlashAttribute("failure", "Selected file is not image.");
			return "redirect:product";
		}
		product.setImgPath(filePath);
		Product productdb = productService.addProduct(product);
		if (productdb != null) {
			model.addFlashAttribute("success", "Action Processed");
		} else {
			model.addFlashAttribute("failure", "Action Process Failed");
		}
		return "redirect:product";
	}

	@RequestMapping(value = "/productedit/{productId}", method = RequestMethod.GET)
	public String edit(@PathVariable("productId") Long Id, ModelMap model) {
		model.addAttribute("productlist", productService.getProductById(Id));
		model.addAttribute("catagorylist", catagoryService.catagorylist());
		model.addAttribute("subcatagorylist", catagoryService.subCatagoryList());
		return "Product/productedit";
	}

	@RequestMapping(value = "/editProduct", method = RequestMethod.POST)
	public String editProduct(@ModelAttribute Product product, @RequestParam("file") MultipartFile file,
			RedirectAttributes model, HttpServletRequest request) {
		Product pmodel = new Product();
		pmodel.setImgPath(product.getImgPath());
		String filePath = "upload/" + file.getOriginalFilename();
		if (file.getOriginalFilename() != null && !file.getOriginalFilename().equals("")) {
			File f = new File(filePath);
			String mimetype = new MimetypesFileTypeMap().getContentType(f);
			if (mimetype.startsWith("image/")) {
				System.out.println("It's an image");
				try {

					byte[] bytes = file.getBytes();
					OutputStream os = new FileOutputStream("src/main/resources/static/" + filePath);
					for (int x = 0; x < bytes.length; x++) {
						os.write(bytes[x]); // writes the bytes
					}
					os.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			} else {
				model.addFlashAttribute("failure", "Selected file is not image.");
				return "redirect:product";
			}

			pmodel.setImgPath(filePath);
		}

		pmodel.setProductId(product.getProductId());
		pmodel.setProductName(product.getProductName());
		pmodel.setProductSize(product.getProductSize());
		pmodel.setProductColor(product.getProductColor());
		pmodel.setQuantity(product.getQuantity());
		pmodel.setDescription(product.getDescription());
		pmodel.setPrice(product.getPrice());
		pmodel.setsCatagory(product.getsCatagory());
		pmodel.setCatagory(product.getCatagory());
		pmodel.setUpdatedBy("");
		pmodel.setUpdatedDt(new Date());

		Product productdb = productService.updateProduct(pmodel);
		if (productdb != null) {
			model.addFlashAttribute("success", "Action Processed");
		} else {
			model.addFlashAttribute("failure", "Action Process Failed");
		}

		return "redirect:product";
	}

	@RequestMapping(value = "/productlist", method = RequestMethod.GET)
	public String productList(ModelMap model) {
		model.addAttribute("productlist", productService.getProducts());
		return "product/productlist";
	}



}
