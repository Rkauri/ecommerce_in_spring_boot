package com.british.product.service;

import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.british.catagory.model.Catagory;
import com.british.catagory.model.SubCatagory;
import com.british.product.dao.ProductRepository;
import com.british.product.model.Product;

@Service
public class ProductServiceImpl implements ProductService {
	@Autowired
	private ProductRepository productRepository;
	private String filepath = "";
	private Pattern pattern;
	private Matcher matcher;

	private static final String IMAGE_PATTERN = "([^\\s]+(\\.(?i)(jpg|png|gif|bmp))$)";

	@Override
	public Product addProduct(Product product) {
		Product productdb = null;
		if (product != null && product.getProductName() != null && !product.getProductName().equals("")) {
			product.setCreatedDt(new Date());
			productdb = productRepository.save(product);
		}
		return productdb;
	}

	@Override
	public List<Product> getProducts() {
		return productRepository.findAll();
	}

	@Override
	public Product getProductById(Long productId) {
		return productRepository.findByProductId(productId);
	}

	@Override
	public String uploadImage(MultipartFile file) {
		// HttpServletRequest request=
		// filepath=request.getRealPath(arg0);
		return null;
	}

	@Override
	public List<Product> getProductLBySubCatagory(SubCatagory subCatagory) {
		return productRepository.findBySCatagory(subCatagory);
	}

	@Override
	public Product updateProduct(Product product) {
		Product result = productRepository.save(product);
		return result;
	}

	@Override
	public List<Product> findLatestProduct() {
		return productRepository.findLatestProduct();
	}

	@Override
	public List<Product> findByCatagory(Catagory catagory) {
		return productRepository.findByCatagory(catagory);
	}

	@Override
	public List<Product> findBySubCatagory(SubCatagory subCatagory) {
		return productRepository.findBySCatagory(subCatagory) ;
	}

	@Override
	public List<Product> getBySubCatagory(Long id, Long firstindex, Long lastindex) {
		List<Product> list=productRepository.getBySCatagory(id,firstindex,lastindex) ;
		return list;
	}
	@Override
	public List<Product> getProductSearch(String productname) {
		List<Product> list=productRepository.findByProductNameContaining(productname) ;
		return list;
	}

}
