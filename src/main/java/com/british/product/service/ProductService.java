package com.british.product.service;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.british.catagory.model.Catagory;
import com.british.catagory.model.SubCatagory;
import com.british.product.model.Product;

@Service
public interface ProductService {
	public Product addProduct(Product product);

	public List<Product> getProducts();

	public Product getProductById(Long productId);

	public String uploadImage(MultipartFile file);

	public List<Product> getProductLBySubCatagory(SubCatagory subCatagory);

	public Product updateProduct(Product product);

	public List<Product> findLatestProduct();

	public List<Product> findByCatagory(Catagory catagory);

	public List<Product> findBySubCatagory(SubCatagory subCatagory);

	public List<Product> getBySubCatagory(Long id, Long firstindex, Long lastindex);
	
	public List<Product> getProductSearch(String productname);
}
