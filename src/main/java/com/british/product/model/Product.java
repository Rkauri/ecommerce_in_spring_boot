package com.british.product.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.british.catagory.model.Catagory;
import com.british.catagory.model.ChildCatagory;
import com.british.catagory.model.SubCatagory;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name = "tbl_product")
public class Product {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE,generator="productSeqGen")
	@SequenceGenerator(name = "productSeqGen", sequenceName = "product_SEQ_GEN")
	private Long productId;
	private String productName;
	private String description;
	private int quantity;
	private BigDecimal price;
	private String productColor;
	private String productSize;
	private String imgPath;
	private Date createdDt, updatedDt;
	private String createdBy, updatedBy;
	@JsonManagedReference(value="product")
	@ManyToOne
	private Catagory catagory;
	@JsonManagedReference(value="sp-catagory")
	@ManyToOne
	private SubCatagory sCatagory;
//	@JsonManagedReference(value="cp-product")
//	@ManyToOne
//	private ChildCatagory cCatagory;
	public Long getProductId() {
		return productId;
	}

	public void setProductId(Long productId) {
		this.productId = productId;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public String getProductColor() {
		return productColor;
	}

	public void setProductColor(String productColor) {
		this.productColor = productColor;
	}

	public String getProductSize() {
		return productSize;
	}

	public void setProductSize(String productSize) {
		this.productSize = productSize;
	}

	public Date getCreatedDt() {
		return createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Date getUpdatedDt() {
		return updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Catagory getCatagory() {
		return catagory;
	}

	public void setCatagory(Catagory catagory) {
		this.catagory = catagory;
	}

	public SubCatagory getsCatagory() {
		return sCatagory;
	}

	public void setsCatagory(SubCatagory sCatagory) {
		this.sCatagory = sCatagory;
	}

//	public ChildCatagory getcCatagory() {
//		return cCatagory;
//	}
//
//	public void setcCatagory(ChildCatagory cCatagory) {
//		this.cCatagory = cCatagory;
//	}

	public String getImgPath() {
		return imgPath;
	}

	public void setImgPath(String imgPath) {
		this.imgPath = imgPath;
	}

	
}
