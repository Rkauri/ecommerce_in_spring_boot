package com.british;

import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.british.security.Role;
import com.british.security.UserRole;
import com.british.user.model.User;
import com.british.user.service.UserService;
import com.british.utility.SecurityUtility;

@SpringBootApplication
public class EcommerceApplication implements CommandLineRunner {

	@Autowired
	private UserService userService;
	
	public static void main(String[] args) {
		SpringApplication.run(EcommerceApplication.class, args);
	}
	@Override
	public void run(String... args) throws Exception {
		User user1 = new User();
		//user1.setId(1L);
		user1.setFirstName("Rabi");
		user1.setLastName("Kauri");
		user1.setUsername("rabi");
		user1.setPassword(SecurityUtility.passwordEncoder().encode("test@123"));
		user1.setEmail("rkauri.rk@gmail.com");
		Set<UserRole> userRoles = new HashSet<>();
		Role role1= new Role();
		role1.setRoleId(1);
		role1.setName("ROLE_ADMIN");
		userRoles.add(new UserRole(user1, role1));
		
		userService.createUser(user1, userRoles);
	}

}
