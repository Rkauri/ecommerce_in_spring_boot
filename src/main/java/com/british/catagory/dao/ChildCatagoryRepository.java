package com.british.catagory.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.british.catagory.model.ChildCatagory;

public interface ChildCatagoryRepository extends JpaRepository<ChildCatagory, Long> {
 ChildCatagory findByChildCatagoryId(Long childCatagoryId);
}
