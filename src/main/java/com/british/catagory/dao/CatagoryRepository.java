package com.british.catagory.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.british.catagory.model.Catagory;

public interface CatagoryRepository  extends JpaRepository<Catagory, Long>{
	Catagory findByCatagoryId(Long catagoryId);
}
