package com.british.catagory.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.british.catagory.model.Catagory;
import com.british.catagory.model.SubCatagory;

public interface SubCatagoryRepository extends JpaRepository<SubCatagory, Long> {
	SubCatagory findBySubCatagoryId(Long subCatagoryId);
	List<SubCatagory> findByCatagoryId(Catagory catagoryId);
}
