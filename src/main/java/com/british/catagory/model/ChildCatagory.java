package com.british.catagory.model;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.british.product.model.Product;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name = "tbl_childcatagory")
public class ChildCatagory {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE,generator="childSeqGen")
	@SequenceGenerator(name = "childSeqGen", sequenceName = "child_SEQ_GEN")
	private long childCatagoryId;
	private String childCatagoryName;
	private Date createdDt,updatedDt;
	private String createdBy,updatedBy;
	@JsonManagedReference(value="child-catagory")
	@ManyToOne
//	private SubCatagory subCatagory;
//	@JsonBackReference(value="cp-catagory")
//	@OneToMany(mappedBy="cCatagory")
//	private List<Product> productlist;

	public long getChildCatagoryId() {
		return childCatagoryId;
	}

	public void setChildCatagoryId(long childCatagoryId) {
		this.childCatagoryId = childCatagoryId;
	}

	public String getChildCatagoryName() {
		return childCatagoryName;
	}

	public void setChildCatagoryName(String childCatagoryName) {
		this.childCatagoryName = childCatagoryName;
	}

//	public SubCatagory getSubCatagory() {
//		return subCatagory;
//	}
//
//	public void setSubCatagory(SubCatagory subCatagory) {
//		this.subCatagory = subCatagory;
//	}

	public Date getCreatedDt() {
		return createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Date getUpdatedDt() {
		return updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

//	public List<Product> getProductlist() {
//		return productlist;
//	}
//
//	public void setProductlist(List<Product> productlist) {
//		this.productlist = productlist;
//	}
	
}
