package com.british.catagory.model;

import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.british.product.model.Product;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "tbl_catagory")
@JsonIgnoreProperties("demo")
public class Catagory {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE,generator="catSeqGen")
	@SequenceGenerator(name = "catSeqGen", sequenceName = "cat_SEQ_GEN")
	private long catagoryId;
	private String catagoryName;
	@JsonBackReference(value="product")
	@OneToMany(mappedBy="catagory")
	private List<Product> productlist;
	private Date createdDt,updatedDt;
	private String createdBy,updatedBy;
	@JsonBackReference(value="sub-catagory")
	@OneToMany(mappedBy = "catagoryId")
	private List<SubCatagory> subCatagoryList;

	public long getCatagoryId() {
		return catagoryId;
	}

	public void setCatagoryId(long catagoryId) {
		this.catagoryId = catagoryId;
	}

	public String getCatagoryName() {
		return catagoryName;
	}

	public void setCatagoryName(String catagoryName) {
		this.catagoryName = catagoryName;
	}

	public List<SubCatagory> getSubCatagoryList() {
		return subCatagoryList;
	}

	public void setSubCatagoryList(List<SubCatagory> subCatagoryList) {
		this.subCatagoryList = subCatagoryList;
	}

	public Date getCreatedDt() {
		return createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Date getUpdatedDt() {
		return updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public List<Product> getProductlist() {
		return productlist;
	}

	public void setProductlist(List<Product> productlist) {
		this.productlist = productlist;
	}
	

}
