package com.british.catagory.model;

import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.british.product.model.Product;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name = "tbl_subcatagory")
public class SubCatagory {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE,generator="subSeqGen")
	@SequenceGenerator(name = "subSeqGen", sequenceName = "sub_SEQ_GEN")
	private long subCatagoryId;
	private String subCatagoryName;
	private Date createdDt,updatedDt;
	private String createdBy,updatedBy;
	@JsonManagedReference(value="sub-catagory")
	@ManyToOne
	private Catagory catagoryId;
//	@JsonBackReference(value="child-catagory")
//	@OneToMany(mappedBy = "subCatagory")
//	private List<ChildCatagory> childCatagorylist;
	@JsonBackReference(value="sp-catagory")
	@OneToMany(mappedBy="sCatagory")
	private List<Product> productlist;

	public long getSubCatagoryId() {
		return subCatagoryId;
	}

	public void setSubCatagoryId(long subCatagoryId) {
		this.subCatagoryId = subCatagoryId;
	}

	public String getSubCatagoryName() {
		return subCatagoryName;
	}

	public void setSubCatagoryName(String subCatagoryName) {
		this.subCatagoryName = subCatagoryName;
	}

	public Catagory getCatagoryId() {
		return catagoryId;
	}

	public void setCatagoryId(Catagory catagoryId) {
		this.catagoryId = catagoryId;
	}

//	public List<ChildCatagory> getChildCatagorylist() {
//		return childCatagorylist;
//	}
//
//	public void setChildCatagorylist(List<ChildCatagory> childCatagorylist) {
//		this.childCatagorylist = childCatagorylist;
//	}

	public Date getCreatedDt() {
		return createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Date getUpdatedDt() {
		return updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public List<Product> getProductlist() {
		return productlist;
	}

	public void setProductlist(List<Product> productlist) {
		this.productlist = productlist;
	}
	

}
