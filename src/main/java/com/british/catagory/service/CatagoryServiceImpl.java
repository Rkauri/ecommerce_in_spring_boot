package com.british.catagory.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.british.catagory.dao.CatagoryRepository;
import com.british.catagory.dao.ChildCatagoryRepository;
import com.british.catagory.dao.SubCatagoryRepository;
import com.british.catagory.model.Catagory;
import com.british.catagory.model.ChildCatagory;
import com.british.catagory.model.SubCatagory;

@Service
public class CatagoryServiceImpl implements CatagoryService {
	@Autowired
	private CatagoryRepository catagoryRepository;
	@Autowired
	private SubCatagoryRepository subCatagoryRepository;
	@Autowired
	private ChildCatagoryRepository childCatagoryRepository;

	@Override
	public boolean addCatagory(Catagory catagory) {
		if (catagory != null && catagory.getCatagoryName() != null && !catagory.getCatagoryName().equals("")) {
			catagory.setCreatedDt(new Date());			
			Catagory catagoryDb = catagoryRepository.save(catagory);
			if (catagoryDb != null) {
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	@Override
	public List<Catagory> catagorylist() {

		return catagoryRepository.findAll();
	}

	@Override
	public Catagory getByCatagoryKey(Long catagoryId) {
		return catagoryRepository.findByCatagoryId(catagoryId);
	}

	@Override
	public boolean addSubCatagory(SubCatagory subCatagory) {
		if (subCatagory != null && subCatagory.getSubCatagoryName() != null && subCatagory.getCatagoryId() != null) {
			subCatagory.setCreatedDt(new Date());
			SubCatagory subCatagoryDb = subCatagoryRepository.save(subCatagory);
			if (subCatagoryDb != null) {
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	@Override
	public List<SubCatagory> subCatagoryList() {

		return subCatagoryRepository.findAll();
	}

	@Override
	public SubCatagory getBySubCatagoryKey(Long subCatagoryId) {

		return subCatagoryRepository.findBySubCatagoryId(subCatagoryId);
	}

	@Override
	public List<SubCatagory> getByCatagoryId(Catagory catagoryId) {

		return subCatagoryRepository.findByCatagoryId(catagoryId);
	}

	

}
