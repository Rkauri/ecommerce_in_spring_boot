package com.british.catagory.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.british.catagory.model.Catagory;
import com.british.catagory.model.ChildCatagory;
import com.british.catagory.model.SubCatagory;

@Service
public interface CatagoryService {
 public boolean addCatagory(Catagory catagory);
 public List<Catagory> catagorylist();
 public Catagory getByCatagoryKey(Long catagoryId);
 public boolean addSubCatagory(SubCatagory subCatagory);
 public List<SubCatagory> subCatagoryList();
 public SubCatagory getBySubCatagoryKey(Long subCatagoryId);
 public List<SubCatagory> getByCatagoryId(Catagory catagoryId);
}
