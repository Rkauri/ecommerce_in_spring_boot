package com.british.payment.service;

import java.util.List;

import com.british.order.model.Order;
import com.british.payment.model.PaymentInfo;
import com.british.user.model.User;

public interface PaymentService {
public void insertPayment(PaymentInfo payment);
public List<PaymentInfo> paymentList();
public List<PaymentInfo> getPaymentById(Long id);
public List<PaymentInfo> getPaymentByUserId(User userId);
public List<PaymentInfo> getPaymentByOrderId(Order orderId);
}
