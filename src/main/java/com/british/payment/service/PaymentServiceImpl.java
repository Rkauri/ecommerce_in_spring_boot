package com.british.payment.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.british.order.model.Order;
import com.british.payment.dao.PaymentRepository;
import com.british.payment.model.PaymentInfo;
import com.british.user.model.User;
@Service
public class PaymentServiceImpl implements PaymentService {
@Autowired
PaymentRepository paymentRepository;
	@Override
	public void insertPayment(PaymentInfo payment) {
		paymentRepository.save(payment);
		
	}

	@Override
	public List<PaymentInfo> paymentList() {
		return paymentRepository.findAll();
	}

	@Override
	public List<PaymentInfo> getPaymentById(Long id) {	
		return paymentRepository.getPaymentById(id);
	}

	@Override
	public List<PaymentInfo> getPaymentByUserId(User userId) {
	
		return paymentRepository.getPaymentByUserId(userId);
	}

	@Override
	public List<PaymentInfo> getPaymentByOrderId(Order orderId) {
			return paymentRepository.getPaymentByOrderId(orderId);
	}
}
