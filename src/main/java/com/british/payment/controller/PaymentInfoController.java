package com.british.payment.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.british.payment.service.PaymentService;

@Controller
@RequestMapping(value="/admin")
public class PaymentInfoController {
@Autowired 
PaymentService paymentService;

@RequestMapping(value="/getPaymentDetail",method=RequestMethod.GET)
public String getPaymentDetail(ModelMap model) {
	model.addAttribute("paymentlist", paymentService.paymentList());
	return "payment/paymentinfo";
}
	
}
