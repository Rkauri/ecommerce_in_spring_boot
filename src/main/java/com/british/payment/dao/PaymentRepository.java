package com.british.payment.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.british.order.model.Order;
import com.british.payment.model.PaymentInfo;
import com.british.user.model.User;

public interface PaymentRepository extends JpaRepository<PaymentInfo,Long> {
	public List<PaymentInfo> getPaymentById(Long id);
	public List<PaymentInfo> getPaymentByUserId(User userId);
	public List<PaymentInfo> getPaymentByOrderId(Order orderId);

}
