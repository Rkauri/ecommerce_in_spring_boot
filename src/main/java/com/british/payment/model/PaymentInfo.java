package com.british.payment.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.british.order.model.Order;
import com.british.user.model.User;

@Entity
@Table(name="tbl_payment")
public class PaymentInfo {
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE,generator="paymentSeqGen")
	@SequenceGenerator(name="paymentSeqGen",sequenceName="pay_Seq_Gen")
	private Long id;
	@OneToOne
	private Order orderId;
	@ManyToOne
	private User userId;
	private String payerId;
	private String status;
	private Date paymentDate;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Order getOrderId() {
		return orderId;
	}
	public void setOrderId(Order orderId) {
		this.orderId = orderId;
	}
	public User getUserId() {
		return userId;
	}
	public void setUserId(User userId) {
		this.userId = userId;
	}
	public String getPayerId() {
		return payerId;
	}
	public void setPayerId(String payerId) {
		this.payerId = payerId;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Date getPaymentDate() {
		return paymentDate;
	}
	public void setPaymentDate(Date paymentDate) {
		this.paymentDate = paymentDate;
	}

}
