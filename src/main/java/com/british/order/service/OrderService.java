package com.british.order.service;

import java.util.Date;
import java.util.List;

import com.british.order.model.Order;
import com.british.order.model.OrderDetail;

public interface OrderService {

	public Order addOrder(Order order);
	public List<Order> getOrders();
	public Order updateOrder(Order order);
	public Order getOrderByKey(Long orderId);
	public void addOrderDetail(OrderDetail orderDetail);
	public List<OrderDetail> getOrderDetailByOrderId(Long orderId);
	public List<Order> getDailyReport();
	public List<Order> getTimelyReport(Date from,Date to);	
}
