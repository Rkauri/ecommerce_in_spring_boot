package com.british.order.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.british.order.dao.OrderDetailRepository;
import com.british.order.dao.OrderRepository;
import com.british.order.model.Order;
import com.british.order.model.OrderDetail;
@Service
public class OrderServiceImpl implements OrderService {
@Autowired
private OrderRepository orderRepository;
@Autowired
private OrderDetailRepository orderDetailRepository;
	
	@Override
	public Order addOrder(Order order) {
		Order db_order=null;
		if(order!=null) {
	//		order.setOrderDate(new Date());
		db_order=orderRepository.save(order);
		}
		
		return db_order;
	}

	@Override
	public List<Order> getOrders() {
	
		return orderRepository.findAll();
	}

	@Override
	public Order updateOrder(Order order) {
		Order db_model=null;
		if(order!=null) {
		 db_model=orderRepository.save(order);	
		}
		return db_model;
	}

	@Override
	public Order getOrderByKey(Long orderId) {
		
		return orderRepository.findOne(orderId);
	}

	@Override
	public void addOrderDetail(OrderDetail orderDetail) {
		orderDetailRepository.save(orderDetail);		
	}

	@Override
	public List<OrderDetail> getOrderDetailByOrderId(Long orderId) {
		Order order= new Order();
		order.setOrderId(orderId);
		return orderDetailRepository.findByOrderId(order);
	}
	@Override
	public List<Order> getDailyReport() {		
		return orderRepository.getDailyReport();
	}
	@Override
	public List<Order> getTimelyReport(Date from,Date to) {		
		return orderRepository.getTimelyReport(from, to);
	}
}
