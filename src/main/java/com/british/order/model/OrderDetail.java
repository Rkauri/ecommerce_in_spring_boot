package com.british.order.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.british.product.model.Product;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="tbl_order_detail")
public class OrderDetail {
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE,generator="orderDetailSeqGen")
	@SequenceGenerator(name="orderDetailSeqGen",sequenceName="order_Dtl_seq_gen")
	private Long orderDetailId;
	@ManyToOne
	@JsonIgnore
	private Order orderId;
	private String detail;
	private int quantity;
	private Date orderDate;
	private String productColor;
	private String productSize;	
	@OneToOne
	private Product productlist;
	public Long getOrderDetailId() {
		return orderDetailId;
	}
	public void setOrderDetailId(Long orderDetailId) {
		this.orderDetailId = orderDetailId;
	}
	public Order getOrderId() {
		return orderId;
	}
	public void setOrderId(Order orderId) {
		this.orderId = orderId;
	}
	public String getDetail() {
		return detail;
	}
	public void setDetail(String detail) {
		this.detail = detail;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	public Date getOrderDate() {
		return orderDate;
	}
	public void setOrderDate(Date orderDate) {
		this.orderDate = orderDate;
	}
	public String getProductColor() {
		return productColor;
	}
	public void setProductColor(String productColor) {
		this.productColor = productColor;
	}
	public String getProductSize() {
		return productSize;
	}
	public void setProductSize(String productSize) {
		this.productSize = productSize;
	}
	public Product getProductlist() {
		return productlist;
	}
	public void setProductlist(Product productlist) {
		this.productlist = productlist;
	}

	
	

}
