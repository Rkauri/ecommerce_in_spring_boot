package com.british.order.model;

import java.io.Serializable;

public class OrderPk implements Serializable {
private Long orderId;
private Long orderDetailId;

@Override
public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((orderDetailId == null) ? 0 : orderDetailId.hashCode());
	result = prime * result + ((orderId == null) ? 0 : orderId.hashCode());
	return result;
}

public OrderPk() {
	super();
}

@Override
public boolean equals(Object obj) {
	if (this == obj)
		return true;
	if (obj == null)
		return false;
	if (getClass() != obj.getClass())
		return false;
	OrderPk other = (OrderPk) obj;
	if (orderDetailId == null) {
		if (other.orderDetailId != null)
			return false;
	} else if (!orderDetailId.equals(other.orderDetailId))
		return false;
	if (orderId == null) {
		if (other.orderId != null)
			return false;
	} else if (!orderId.equals(other.orderId))
		return false;
	return true;
}
public Long getOrderId() {
	return orderId;
}
public void setOrderId(Long orderId) {
	this.orderId = orderId;
}
public Long getOrderDetailId() {
	return orderDetailId;
}
public void setOrderDetailId(Long orderDetailId) {
	this.orderDetailId = orderDetailId;
}

}
