package com.british.order.controller;

import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.british.order.model.Order;
import com.british.order.service.OrderService;
import com.british.user.service.UserService;

@Controller
@RequestMapping(value = "/admin")
public class OrderController {
	@Autowired
	private OrderService orderService;
	@Autowired
	private UserService userService;

	@RequestMapping(value = "/orderlist", method = RequestMethod.GET)
	public String getOrderList(ModelMap model) {
		model.addAttribute("orderlist", orderService.getOrders());
		return "order/orderlist";
	}

	@RequestMapping(value = "/orderlist/{orderId}", method = RequestMethod.GET)
	public String getOrderDetail(@PathVariable("orderId") Long id, ModelMap model) {
		model.addAttribute("orderlist", orderService.getOrderDetailByOrderId(id));
		return "Order/orderdetaillist";
	}

	@RequestMapping(value = "/changeStatus", method = RequestMethod.POST)
	public @ResponseBody HashMap<String, String> changeStatus(@RequestParam("orderid") Long id,
			@RequestParam("status") String status) {
		HashMap<String, String> hmap = new HashMap<>();
		Order obj = orderService.getOrderByKey(id);
		obj.setStatus(status);
		Order o = orderService.updateOrder(obj);
		if (o.getStatus().equals(status)) {
			hmap.put("msg", "Order Status Updated Successfully");
		} else {
			hmap.put("msg", "Fail To Change Status");
		}
		return hmap;
	}

}
