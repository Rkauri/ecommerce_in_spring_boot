package com.british.order.dao;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.british.order.model.Order;

public interface OrderRepository extends JpaRepository<Order, Long>{
	@Query(value="select * from tbl_order where TO_DATE(CAST(order_date AS DATE),'YYYY-MM-DD')=to_date(sysdate,'YYYY-MM-DD')",nativeQuery=true)
	public List<Order> getDailyReport();
	@Query(value="select * from tbl_order where to_date(cast(order_date as date),'dd-mon-yy') >= ?1 and to_date(cast(order_date as date),'dd-mon-yy')<= ?2",nativeQuery=true)
	public List<Order> getTimelyReport(Date from,Date to);
}
