package com.british.order.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.british.order.model.Order;
import com.british.order.model.OrderDetail;

public interface OrderDetailRepository extends JpaRepository<OrderDetail, Long> {
	List<OrderDetail> findByOrderId(Order orderId);
}
