package com.british.paypal.controller;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.british.order.model.Order;
import com.british.order.model.OrderDetail;
import com.british.order.service.OrderService;
import com.british.payment.model.PaymentInfo;
import com.british.payment.service.PaymentService;
import com.british.paypal.config.PaypalPaymentIntent;
import com.british.paypal.config.PaypalPaymentMethod;
import com.british.paypal.service.PaypalService;
import com.british.paypal.util.URLUtils;
import com.british.product.model.Product;
import com.british.product.service.ProductService;
import com.british.user.model.User;
import com.british.user.service.UserService;
import com.paypal.api.payments.Links;
import com.paypal.api.payments.Payment;
import com.paypal.base.rest.PayPalRESTException;

@Controller
@RequestMapping("/")
public class PaymentController {

	public static final String PAYPAL_SUCCESS_URL = "pay/success";
	public static final String PAYPAL_CANCEL_URL = "pay/cancel";
	public static Long orderId = 0L;
	private Logger log = LoggerFactory.getLogger(getClass());

	@Autowired
	private PaypalService paypalService;
	@Autowired
	private ProductService productService;
	@Autowired
	private UserService userService;
	@Autowired
	private OrderService orderService;
	@Autowired
	private PaymentService paymentService;

	@RequestMapping(method = RequestMethod.GET, value = "/testing")
	public String index() {
		return "paypal/index";
	}

	@RequestMapping(method = RequestMethod.POST, value = "/pay")
	public String pay(HttpServletRequest request, Authentication auth) {
		String cancelUrl = URLUtils.getBaseURl(request) + "/" + PAYPAL_CANCEL_URL;
		String successUrl = URLUtils.getBaseURl(request) + "/" + PAYPAL_SUCCESS_URL;
		BigDecimal totalprice = new BigDecimal(0);
		try {

			HttpSession session = request.getSession(false);
			List<Product> productlist = (List<Product>) session.getAttribute("product");
			Order order = new Order();
			order.setOrderDate(new Date());
			order.setStatus("conforme");
			if (auth.getName() != null) {
				User u = userService.findByUsername(auth.getName());
				order.setUserId(u);
				Order dbmodel = orderService.addOrder(order);
				orderId=dbmodel.getOrderId();
				if (dbmodel.getOrderId() != null) {
					for (Product p : productlist) {
						OrderDetail d = new OrderDetail();
						d.setOrderDate(new Date());
						d.setProductlist(p);
						d.setOrderId(dbmodel);
						d.setQuantity(p.getQuantity());
						orderService.addOrderDetail(d);
						int qty;
						BigDecimal price = new BigDecimal(0);
						Product db_model = productService.getProductById(p.getProductId());
						qty = p.getQuantity();
						price = db_model.getPrice();
						price = price.multiply(new BigDecimal(qty));
						totalprice = totalprice.add(price, MathContext.DECIMAL64);
					}
				}
				totalprice = totalprice.divide(new BigDecimal(108), 2, RoundingMode.HALF_UP);
				session.removeAttribute("produdct");

				Payment payment = paypalService.createPayment(totalprice.doubleValue(), "USD",
						PaypalPaymentMethod.paypal, PaypalPaymentIntent.sale, "payment description", cancelUrl,
						successUrl);
				for (Links links : payment.getLinks()) {
					if (links.getRel().equals("approval_url")) {
						return "redirect:" + links.getHref();
					}
				}

			} else {
				return "/";
			}
		} catch (PayPalRESTException e) {
			log.error(e.getMessage());
		}
		return "redirect:testing";
	}

	@RequestMapping(method = RequestMethod.GET, value = PAYPAL_CANCEL_URL)
	public String cancelPay() {
		return "paypal/cancel";
	}

	@RequestMapping(method = RequestMethod.GET, value = PAYPAL_SUCCESS_URL)
	public String successPay(@RequestParam("paymentId") String paymentId, @RequestParam("PayerID") String payerId,
			Authentication auth,ModelMap model) {
		try {
			Payment payment = paypalService.executePayment(paymentId, payerId);
			if (payment.getState().equals("approved")) {
				Order o = orderService.getOrderByKey(orderId);
				User u = userService.findByUsername(auth.getName());
				PaymentInfo pay = new PaymentInfo();
				pay.setOrderId(o);
				pay.setUserId(u);
				pay.setPayerId(payerId);
				pay.setStatus("Paid");
				pay.setPaymentDate(new Date());
				paymentService.insertPayment(pay);
				model.addAttribute("success", "payment successful");
				return "client/index";
			}else {
				model.addAttribute("failure", "payment failed");
			}
		} catch (PayPalRESTException e) {
			log.error(e.getMessage());
		}
		return "client/index";
	}

}
