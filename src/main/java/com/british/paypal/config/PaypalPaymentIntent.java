package com.british.paypal.config;

public enum PaypalPaymentIntent {

	sale, authorize, order
	
}
