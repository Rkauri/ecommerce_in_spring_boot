package com.british.report.controller;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.british.order.model.Order;
import com.british.order.service.OrderService;

@Controller
@RequestMapping(value="/admin")
public class SalesReportController {
@Autowired
OrderService orderService;

@RequestMapping(value="/daily-report",method=RequestMethod.GET)
public String getDailyReport(ModelMap model){
	model.addAttribute("orderlist", orderService.getDailyReport());
	return "report/dailyreport";
}
@RequestMapping(value="/timely-report",method=RequestMethod.GET)
public String gettimelyReport(){
	//model.addAttribute("orderlist", orderService.getTimelyReport(from,to));
	return "report/timelyreport";
}
@RequestMapping(value="/getReport",method=RequestMethod.GET)
public @ResponseBody  List<Order> gettimelyReport(@RequestParam("from") Date from,@RequestParam("to") Date to,ModelMap model){
	//HashMap<String, List<Order>> hmap = new HashMap<>();
	List<Order> list=orderService.getTimelyReport(from,to);
	
	//hmap.put("orderlist",list);
	return list;
}





}
