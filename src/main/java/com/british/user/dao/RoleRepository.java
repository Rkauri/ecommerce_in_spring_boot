package com.british.user.dao;

import org.springframework.data.repository.CrudRepository;

import com.british.security.Role;

public interface RoleRepository extends CrudRepository<Role, Long> {
	Role findByname(String name);
}
