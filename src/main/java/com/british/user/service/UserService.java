package com.british.user.service;

import java.util.Set;

import com.british.security.PasswordResetToken;
import com.british.security.UserRole;
import com.british.user.model.User;

public interface UserService {
	PasswordResetToken getPasswordResetToken(final String token);
	
	void createPasswordResetTokenForUser(final User user, final String token);
	
	User findByUsername(String username);
	
	User findByEmail (String email);
	
	User findById(Long id);
	
	User createUser(User user, Set<UserRole> userRoles) throws Exception;
	
	User save(User user);

}
